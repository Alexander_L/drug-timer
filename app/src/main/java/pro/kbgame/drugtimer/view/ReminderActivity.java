package pro.kbgame.drugtimer.view;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pro.kbgame.drugtimer.R;
import pro.kbgame.drugtimer.model.Reminder;
import pro.kbgame.drugtimer.model.RemindersManager;
import pro.kbgame.drugtimer.model.Time;
import pro.kbgame.drugtimer.utils.LocalWeekDaysNames;

public class ReminderActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    //region <Butterknife binds>
    @BindView(R.id.et_name)
    TextInputEditText etName;
    @BindView(R.id.bt_edit)
    Button btEdit;
    @BindView(R.id.bt_save)
    Button btSave;
    @BindView(R.id.bt_cancel)
    Button btCancel;
    @BindView(R.id.tv_remind)
    TextView tvRemind;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.ch_mon)
    Chip chMon;
    @BindView(R.id.ch_tue)
    Chip chTue;
    @BindView(R.id.ch_wed)
    Chip chWed;
    @BindView(R.id.ch_thu)
    Chip chThu;
    @BindView(R.id.ch_fri)
    Chip chFri;
    @BindView(R.id.ch_sat)
    Chip chSat;
    @BindView(R.id.ch_sun)
    Chip chSun;
    @BindView(R.id.cg_days)
    ChipGroup cgDays;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.ib_add_time)
    ImageButton ibAddTime;
    @BindView(R.id.cg_time)
    ChipGroup cgTime;
    @BindView(R.id.cl_remind)
    ConstraintLayout clRemind;

    @BindViews({
            R.id.ch_mon,
            R.id.ch_tue,
            R.id.ch_wed,
            R.id.ch_thu,
            R.id.ch_fri,
            R.id.ch_sat,
            R.id.ch_sun
    })
    List<Chip> daysChipsList;
    //endregion

    private int position;
    private OpenMode openMode;
    private Reminder reminder;

    @NonNull
    public static Intent makeIntent(@NonNull Activity activity, Reminder reminder, OpenMode openMode) {
        Intent intent = new Intent(activity, ReminderActivity.class);
        intent.putExtra("reminder", reminder);
        intent.putExtra("mode", openMode);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remind_settings);
        ButterKnife.bind(this);

        init();

        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.getExtras() != null) {
                if (intent.getExtras().get("mode") != null) {
                    openMode = (OpenMode) intent.getExtras().get("mode");
                    if (openMode.equals(OpenMode.VIEW) || openMode.equals(OpenMode.EDIT)) {
                        reminder = (Reminder) intent.getSerializableExtra("reminder");
                        position = intent.getIntExtra("position", 0);
                    }
                } else {
                    //show error message
                }

            }
        }
        initUI();
    }


    @OnClick({R.id.bt_edit, R.id.bt_save, R.id.bt_cancel, R.id.ib_add_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_edit:
                editMode();
                break;
            case R.id.bt_save:
                collectsDataFromFields();
                if (checkForEmptyFields()) {
                    saveData();
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case R.id.bt_cancel:
                finish();
                break;
            case R.id.ib_add_time:
                showTimePickerDialog();
                break;

        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        if (isTimeDoubled(hour, minute)) {
            Toast.makeText(this, R.string.time_like_this_is_existing, Toast.LENGTH_SHORT).show();
        } else {
            Time time = new Time(hour, minute, true);
            addTimeChip(time);
        }
    }

    private boolean isTimeDoubled(int hour, int minute) {
        int count = cgTime.getChildCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                Chip chip = (Chip) cgTime.getChildAt(i);
                String timeOnChip = String.valueOf(chip.getText());
                int[] splittedHoursAndMinutesFromTimeChip = new int[2];
                String[] textValuesFromChip = timeOnChip.split(":");
                splittedHoursAndMinutesFromTimeChip[0] = Integer.valueOf(textValuesFromChip[0]); //hours value
                splittedHoursAndMinutesFromTimeChip[1] = Integer.valueOf(textValuesFromChip[1]); //minutes value
                if (splittedHoursAndMinutesFromTimeChip[0] == hour && splittedHoursAndMinutesFromTimeChip[1] == minute) {
                    return true;
                }
            }
        }
        return false;
    }

    private void showTimePickerDialog() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, 0, 0, true);
        timePickerDialog.show();
    }

    private void init() {
        reminder = new Reminder(true, "", new boolean[RemindersManager.getInstance().getDAYS_IN_WEEK()], new ArrayList<>());
    }

    private void initUI() {
        fillFields(reminder);
        fillDaysChips();
        if (openMode.equals(OpenMode.VIEW)) {
            viewMode();
        } else if (openMode.equals(OpenMode.EDIT)) {
            editMode();
        } else {
            createMode();
        }
    }

    private void fillDaysChips() {
        ArrayList<String> daysNames = getLocalWeekDaysNames();
        for (int i = 0; i < daysChipsList.size(); i++) {
            daysChipsList.get(i).setText(daysNames.get(i));
        }
    }

    private ArrayList<String> getLocalWeekDaysNames() {
        //Think here, if user change local at application work time
        return LocalWeekDaysNames.getInstance().getDaysNames("full");
    }

    private void createMode() {
        btEdit.setVisibility(View.INVISIBLE);

    }

    private void viewMode() {
        btSave.setVisibility(View.INVISIBLE);
        btCancel.setVisibility(View.INVISIBLE);
        btEdit.setVisibility(View.VISIBLE);
        ibAddTime.setVisibility(View.INVISIBLE);
        setFieldsForEdit(false);
    }

    private void editMode() {
        openMode = OpenMode.EDIT;
        btSave.setVisibility(View.VISIBLE);
        btCancel.setVisibility(View.VISIBLE);
        btEdit.setVisibility(View.INVISIBLE);
        ibAddTime.setVisibility(View.VISIBLE);
        setFieldsForEdit(true);
    }

    private void setFieldsForEdit(boolean isEdit) {
        etName.setEnabled(isEdit);
        for (Chip chip : daysChipsList) {
            chip.setClickable(isEdit);
        }
        if (cgTime.getChildCount() > 0) {
            for (int i = 0; i < cgTime.getChildCount(); i++) {

                Chip chip = (Chip) cgTime.getChildAt(i);
                chip.setCloseIconVisible(isEdit);
                cgTime.getChildAt(i).setClickable(isEdit);
            }
        }
    }

    private void fillFields(Reminder reminder) {
        etName.setText(reminder.getDrugName());
        setSelectedDaysOfWeek(reminder);
        setTimeChips(reminder);
    }

    private void setTimeChips(Reminder reminder) {
        if (reminder.getTimeList().size() > 0) {
            for (Time time : reminder.getTimeList()) {
                addTimeChip(time);
            }
        }
    }

    private void addTimeChip(Time time) {
        String hourString = String.valueOf(time.getHours());
        String minuteString = String.valueOf(time.getMinutes());
        if (time.getHours() < 10) {
            hourString = "0" + hourString;
        }
        if (time.getMinutes() < 10) {
            minuteString = "0" + minuteString;
        }

        String timeString = hourString + ":" + minuteString;
        Chip chip = (Chip) getLayoutInflater().inflate(R.layout.chip_time, cgTime, false);
        chip.setText(timeString);
        chip.setChecked(time.isActive());
        chip.setOnCheckedChangeListener((compoundButton, b) -> {
            if (time.isActive()) {
                time.setActive(false);
            } else {
                time.setActive(true);
            }
        });
        chip.setOnCloseIconClickListener(view -> {
            reminder.getTimeList().remove(time);
            cgTime.removeView(chip);
        });
        cgTime.addView(chip);
    }

    private void setSelectedDaysOfWeek(Reminder reminder) {
        chMon.setChecked(reminder.getTurnonInWeekDay()[0]);
        chTue.setChecked(reminder.getTurnonInWeekDay()[1]);
        chWed.setChecked(reminder.getTurnonInWeekDay()[2]);
        chThu.setChecked(reminder.getTurnonInWeekDay()[3]);
        chFri.setChecked(reminder.getTurnonInWeekDay()[4]);
        chSat.setChecked(reminder.getTurnonInWeekDay()[5]);
        chSun.setChecked(reminder.getTurnonInWeekDay()[6]);
    }

    private void collectsDataFromFields() {
        reminder.setDrugName(etName.getText().toString());
        reminder.setTurnonInWeekDay(collectSelectedDaysOfWeek());
        reminder.setTimeList(collectTimes());
    }

    private ArrayList<Time> collectTimes() {
        ArrayList<Time> timeList = new ArrayList<>();
        if (cgTime.getChildCount() > 0) {
            for (int i = 0; i < cgTime.getChildCount(); i++) {
                Chip chip = (Chip) cgTime.getChildAt(i);
                String timeString = (String) chip.getText().toString();
                Time time = getTimeFromChip(timeString, chip.isChecked());
                timeList.add(time);
            }
        }
        return timeList;
    }

    private Time getTimeFromChip(String timeString, boolean checked) {
        int[] hourAndMinute = hourAndMinuteFromString(timeString);
        return new Time(hourAndMinute[0], hourAndMinute[1], checked);
    }

    private int[] hourAndMinuteFromString(String timeString) {
        String[] hourAndMinute = timeString.split(":");
        int hour = Integer.parseInt(hourAndMinute[0]);
        int minute = Integer.parseInt(hourAndMinute[1]);
        return new int[]{hour, minute};
    }

    private boolean[] collectSelectedDaysOfWeek() {
        boolean[] selectedDaysOfWeek = new boolean[RemindersManager.getInstance().getDAYS_IN_WEEK()];
        selectedDaysOfWeek[0] = chMon.isChecked();
        selectedDaysOfWeek[1] = chTue.isChecked();
        selectedDaysOfWeek[2] = chWed.isChecked();
        selectedDaysOfWeek[3] = chThu.isChecked();
        selectedDaysOfWeek[4] = chFri.isChecked();
        selectedDaysOfWeek[5] = chSat.isChecked();
        selectedDaysOfWeek[6] = chSun.isChecked();
        return selectedDaysOfWeek;
    }

    private void saveData() {
        if (openMode.equals(OpenMode.CREATE)) {
            RemindersManager.getInstance().addReminderToList(reminder);
        } else if (openMode.equals(OpenMode.EDIT)) {
            RemindersManager.getInstance().saveReminderInList(position, reminder);
        } else {
            //show error message
        }
    }

    private boolean checkForEmptyFields() {
        if (etName.getText().toString().isEmpty()) {
            etName.requestFocus();
            return false;
        } else if (!hasCheckedChips(cgDays)) {
            cgDays.requestFocus();
            return false;
        } else if (!hasCheckedChips(cgTime)) {
            ibAddTime.requestFocus();
            return false;
        }
        return true;
    }

    private boolean hasCheckedChips(ChipGroup chipGroup) {
        if (chipGroup.getChildCount() > 0) {
            for (int i = 0; i < chipGroup.getChildCount(); i++) {
                Chip chip = (Chip) chipGroup.getChildAt(i);
                if (chip.isChecked()) {
                    return true;
                }
            }
        }
        return false;
    }

}

