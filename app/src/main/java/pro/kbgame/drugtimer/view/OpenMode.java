package pro.kbgame.drugtimer.view;

public enum OpenMode {
    CREATE, VIEW, EDIT
}
