package pro.kbgame.drugtimer.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pro.kbgame.drugtimer.R;
import pro.kbgame.drugtimer.model.AlarmsManager;
import pro.kbgame.drugtimer.model.Reminder;
import pro.kbgame.drugtimer.model.RemindersManager;

public class AlertActivity extends AppCompatActivity {
    //region <Butterknife binds>
    @BindView(R.id.tv_drug_name)
    TextView tvDrugName;
    @BindView(R.id.bt_accept)
    Button btAccept;
    //endregion

    private Reminder reminder;

    @NonNull
    public static Intent makeIntent(@NonNull Activity activity, Reminder reminder) {
        Intent intent = new Intent(activity, AlertActivity.class);
        intent.putExtra("reminder", reminder);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.getExtras() != null) {
                if (intent.getExtras().get("reminder") != null) {
                    reminder = (Reminder) intent.getSerializableExtra("reminder");
                } else if (intent.getStringExtra("drugname") != null) {
                    String drugname = intent.getStringExtra("drugname");
                    tvDrugName.setText(drugname);

                } else {
                    reminder = new Reminder(true, "", new boolean[RemindersManager.getInstance().getDAYS_IN_WEEK()], new ArrayList<>());
                    //show empty alert
                }
            }
        }

        initUI();

    }

    private void initUI() {
        if (reminder != null) {
            tvDrugName.setText(reminder.getDrugName());
        }
    }

    @OnClick(R.id.bt_accept)
    public void onViewClicked() {
        AlarmsManager.getInstance().reAlarmExpiredActiveAlarms();
        setResult(RESULT_OK);
        finish();
    }
}
