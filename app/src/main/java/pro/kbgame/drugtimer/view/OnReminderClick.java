package pro.kbgame.drugtimer.view;

import pro.kbgame.drugtimer.model.Reminder;

public interface OnReminderClick {

    void onEditClick(Reminder reminder, int position);

    void onRemoveClick(Reminder reminder);

    void onItemClick(Reminder reminder, int position);

}
