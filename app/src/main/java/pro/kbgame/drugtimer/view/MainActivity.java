package pro.kbgame.drugtimer.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.kbgame.drugtimer.R;
import pro.kbgame.drugtimer.model.ActiveAlarmsRepository;
import pro.kbgame.drugtimer.model.Reminder;
import pro.kbgame.drugtimer.model.RemindersManager;

public class MainActivity extends AppCompatActivity implements OnReminderClick, RemindersManager.OnLoadData {

    @BindView(R.id.rv_reminders)
    RecyclerView rvReminders;

    public static final int EDIT = 101;
    public static final int ALARM = 102;
    RemindersAdapter remindersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActiveAlarmsRepository.getInstance().getActiveAlarms();

        initUI();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = ReminderActivity.makeIntent(MainActivity.this, null, OpenMode.CREATE);
            startActivityForResult(intent, EDIT);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT) {
            if (resultCode == RESULT_OK) {
                remindersAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onEditClick(Reminder reminder, int position) {
        Intent intent = ReminderActivity.makeIntent(this, reminder, OpenMode.EDIT);
        intent.putExtra("position", position);
        startActivityForResult(intent, EDIT);
    }

    @Override
    public void onRemoveClick(Reminder reminder) {
        RemindersManager.getInstance().removeReminderFromList(reminder);
        remindersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Reminder reminder, int position) {
        Intent intent = ReminderActivity.makeIntent(this, reminder, OpenMode.VIEW);
        intent.putExtra("position", position);
        startActivityForResult(intent, EDIT);
    }

    private void initUI() {
        RequestDataUtil.showBlocker(this);
        RemindersManager.getInstance().loadRemindersFromMemory(this);
    }

    @Override
    public void onLoad(ArrayList<Reminder> remindersList) {
        remindersAdapter = new RemindersAdapter(remindersList, this, this);
        rvReminders.setAdapter(remindersAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvReminders.setLayoutManager(linearLayoutManager);
        RequestDataUtil.hideBlocker();
    }
}
