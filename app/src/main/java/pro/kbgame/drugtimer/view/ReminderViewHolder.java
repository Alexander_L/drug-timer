package pro.kbgame.drugtimer.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.kbgame.drugtimer.R;
import pro.kbgame.drugtimer.model.Reminder;
import pro.kbgame.drugtimer.model.RemindersManager;
import pro.kbgame.drugtimer.model.Time;
import pro.kbgame.drugtimer.utils.LocalWeekDaysNames;

class ReminderViewHolder extends RecyclerView.ViewHolder {
    //region <Butterknife binds>
    @BindView(R.id.tv_days)
    TextView tvSelectedDays;
    @BindView(R.id.tv_drug_name)
    TextView tvDrugName;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.sw_remind)
    Switch swRemind;
    @BindView(R.id.ib_edit)
    ImageButton ibEdit;
    @BindView(R.id.ib_delete)
    ImageButton ibDelete;
    //endregion

    ReminderViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bind(Reminder reminder, int position, OnReminderClick onReminderClick, Context context) {
        if (reminder != null) {
            tvDrugName.setText(reminder.getDrugName());
            swRemind.setChecked(reminder.isActive());
            String times = getTimesInString(reminder.getTimeList());
            tvTime.setText(times);

            //TODO set function buttons state (ibEdit, etc.)

            bindListeners(reminder, onReminderClick, position);
            setSelectedDays(reminder, context);

        } else {
            tvDrugName.setText("");
            //set function buttons state (ibEdit, etc.)
            //Need to think about reaction for this state.
        }
    }

    private String getTimesInString(ArrayList<Time> timeList) {
        String fullTimesString = "";
        if (timeList != null && timeList.size() > 0) {
            for (Time time : timeList) {
                if (fullTimesString.length() == 0 && time.isActive()) {
                    fullTimesString = getStringFromTime(time);

                } else if (time.isActive()) {
                    String timeString = getStringFromTime(time);
                    fullTimesString = fullTimesString + "\n" + timeString;
                }
            }
        }
        return fullTimesString;
    }

    private String getStringFromTime(Time time) {
        String hourString = String.valueOf(time.getHours());
        String minuteString = String.valueOf(time.getMinutes());
        if (time.getHours() < 10) {
            hourString = "0" + hourString;
        }
        if (time.getMinutes() < 10) {
            minuteString = "0" + minuteString;
        }

        return hourString + ":" + minuteString;
    }

    private void setSelectedDays(Reminder reminder, Context context) {
        ArrayList<String> selectedDaysList = parseDaysToList(reminder, context);
        String selectedDays = daysStringBuilder(selectedDaysList);
        tvSelectedDays.setText(selectedDays);

    }

    private String daysStringBuilder(ArrayList<String> selectedDaysList) {
        StringBuilder stringBuilder = new StringBuilder();
        if (selectedDaysList.size() != 0 && selectedDaysList.size() > 1) {
            stringBuilder.append(selectedDaysList.get(0));
            String prefix = ", ";
            for (int i = 1; i < selectedDaysList.size(); i++) { //First value must be without comma and space
                String buildingString = prefix.concat(selectedDaysList.get(i));
                stringBuilder.append(buildingString);
            }
        } else if (selectedDaysList.size() == 1) {
            return selectedDaysList.get(0);
        }
        return stringBuilder.toString();
    }

    private ArrayList<String> parseDaysToList(Reminder reminder, Context context) {
        ArrayList<String> selectedDaysList = new ArrayList<>();
        ArrayList<String> localDaysNames = LocalWeekDaysNames.getInstance().getDaysNames("short");
        for (int i = 0; i < reminder.getTurnonInWeekDay().length; i++) {
            if (reminder.getTurnonInWeekDay()[i]) {
                selectedDaysList.add(localDaysNames.get(i));
            }
        }
        return selectedDaysList;
    }

    private void bindListeners(Reminder reminder, OnReminderClick onReminderClick, int position) {
        ibEdit.setOnClickListener(view -> onReminderClick.onEditClick(reminder, position));

        ibDelete.setOnClickListener(view -> onReminderClick.onRemoveClick(reminder));

        itemView.setOnClickListener(view -> onReminderClick.onItemClick(reminder, position));

        swRemind.setOnClickListener(view -> {
            if (swRemind.isChecked()) {
                reminder.setActive(true);
            } else {
                reminder.setActive(false);
            }
            RemindersManager.getInstance().changeReminderActivityState(reminder);
        });
    }
}
