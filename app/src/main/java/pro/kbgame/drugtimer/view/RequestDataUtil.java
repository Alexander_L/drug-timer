package pro.kbgame.drugtimer.view;

import android.app.ProgressDialog;
import android.content.Context;

public class RequestDataUtil {
    private static ProgressDialog progressDialog;

    public static void showBlocker(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideBlocker() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
