package pro.kbgame.drugtimer.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pro.kbgame.drugtimer.R;
import pro.kbgame.drugtimer.model.Reminder;

public class RemindersAdapter extends RecyclerView.Adapter<ReminderViewHolder> {
    private ArrayList<Reminder> remindersList;
    private OnReminderClick onReminderClick;
    private Context context;

    RemindersAdapter(ArrayList<Reminder> remindersList, OnReminderClick onReminderClick, Context context) {
        this.remindersList = remindersList;
        this.onReminderClick = onReminderClick;
        this.context = context;
    }

    @NonNull
    @Override
    public ReminderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reminder, parent, false);
        return new ReminderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderViewHolder holder, int position) {
        Reminder reminder = remindersList.get(position);
        holder.bind(reminder, position, onReminderClick, context);
    }

    @Override
    public int getItemCount() {
        if (remindersList == null) {
            return 0;
        } else
            return remindersList.size();
    }
}
