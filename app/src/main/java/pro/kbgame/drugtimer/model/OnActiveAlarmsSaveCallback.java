package pro.kbgame.drugtimer.model;

import java.util.ArrayList;

public interface OnActiveAlarmsSaveCallback {
    void onActiveAlarmsSave(ArrayList<ActiveAlarm> activeAlarms);
    void onActiveAlarmsSaveError();
}
