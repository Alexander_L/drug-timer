package pro.kbgame.drugtimer.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import pro.kbgame.MyApplication;
import pro.kbgame.drugtimer.utils.AlarmsReceiver;
import pro.kbgame.drugtimer.utils.AlarmsTimersPlanner;

public class AlarmsManager {
    public static volatile AlarmsManager instance = null;
    private Context context;
    private final static String NAME_SPLITTER = ", ";
    private final long MILLISECONDS_IN_WEEK = 604800000; // 7d * 24 h * 60 min *60 sec *1000 milliseconds

    @NonNull
    public static AlarmsManager getInstance() {
        AlarmsManager alarmsManager = instance;
        if (alarmsManager == null) {
            synchronized (AlarmsManager.class) {
                alarmsManager = instance;
                if (alarmsManager == null) {
                    alarmsManager = instance = new AlarmsManager();
                }
            }
        }
        return instance;
    }

    private AlarmsManager() {
        context = MyApplication.getAppContext();
    }

    public void setAlarms(Reminder reminder) {
        String drugName = reminder.getDrugName();
        AlarmsTimersPlanner alarmsTimersPlanner = new AlarmsTimersPlanner(reminder.getTurnonInWeekDay(), reminder.getTimeList());
        ArrayList<Long> alarmsTimesInMilliseconds = alarmsTimersPlanner.getAlarmsTimesInMilliseconds();
        ArrayList<ActiveAlarm> activeAlarms = new ArrayList<>();
        for (long time : alarmsTimesInMilliseconds) {
            ActiveAlarm activeAlarm = new ActiveAlarm(drugName, time);
            activeAlarms.add(activeAlarm);
        }
        planAlarms(activeAlarms);
        for(ActiveAlarm activeAlarm : activeAlarms){
            saveActiveAlarm(activeAlarm);
        }
    }

    private void saveActiveAlarm(ActiveAlarm activeAlarm) {
        ActiveAlarmsRepository.getInstance().setActiveAlarm(activeAlarm);

    }

    public void planAlarms(ArrayList<ActiveAlarm> activeAlarms) {
        for (ActiveAlarm activeAlarm : activeAlarms) {
            if (isAlarmTimeDoubled(activeAlarm.getTime())) {
                //take other active drug in planner
                //connect names for together displaying
                //cancel other alarm
                //remove other alarm from active alarms repository
                //set new alarm with two drug names
                //save it in active alarms repository
                int doubledAlarmNumber = getDoubledAlarmNumber(activeAlarm.getTime());
                ActiveAlarm duplicateAlarm = ActiveAlarmsRepository.getInstance().getActiveAlarms().get(doubledAlarmNumber);
                String newName = activeAlarm.getName() + NAME_SPLITTER + duplicateAlarm.getName();
                activeAlarm.setName(newName);
                cancelAlarm(duplicateAlarm);
                ActiveAlarmsRepository.getInstance().removeAlarm(duplicateAlarm);

            }
            setAlarmToPlanner(activeAlarm);
        }
    }

    private boolean isAlarmTimeDoubled(long alarmTime) {
        //load all active alarms from active alarms repository
        //Think here, may be more optimal way! Check each alarm from repo with income alarmTime.
        ArrayList<ActiveAlarm> activeAlarms = ActiveAlarmsRepository.getInstance().getActiveAlarms();
        for (int i = 0; i < activeAlarms.size(); i++) {
            if (activeAlarms.get(i).getTime() == alarmTime) {
                return true;
            }
        }
        return false;
    }

    private int getDoubledAlarmNumber(long alarmTime) {
        int doubledAlarmNumber = 0;
        ArrayList<ActiveAlarm> activeAlarms = ActiveAlarmsRepository.getInstance().getActiveAlarms();
        for (int i = 0; i < activeAlarms.size(); i++) {
            if (activeAlarms.get(i).getTime() == alarmTime) {
                doubledAlarmNumber = i;
                break;
            }
        }
        return doubledAlarmNumber;
    }

    public void cancelAlarms(Reminder reminder) {
        //check here if drug doubled
        //if doubled - separate with other
        //cancel alarm with two drugs
        //remove alarm from active alarms repositorydoubledAlarmNumber
        //set new alarm with other drug
        //save changes in active alarms repository
        AlarmsTimersPlanner alarmsTimersPlanner = new AlarmsTimersPlanner(reminder.getTurnonInWeekDay(), reminder.getTimeList());
        ArrayList<Long> alarmsTimesInMilliseconds = alarmsTimersPlanner.getAlarmsTimesInMilliseconds();
        for (long reminderActiveAlarm : alarmsTimesInMilliseconds) {
            if (isAlarmTimeDoubled(reminderActiveAlarm)) {
                int doubledAlarmNumber = getDoubledAlarmNumber(reminderActiveAlarm);
                ActiveAlarm activeAlarmForCancel = ActiveAlarmsRepository.getInstance().getActiveAlarms().get(doubledAlarmNumber);
                if (isContainsSomeDrugsNames(activeAlarmForCancel)) {
                    ActiveAlarm activeAlarmForReSet = splitAndRemoveDrugNameFromActiveAlarm(activeAlarmForCancel, reminder.getDrugName());
                    setAlarmToPlanner(activeAlarmForReSet);

                }
                cancelAlarm(activeAlarmForCancel);
                ActiveAlarmsRepository.getInstance().removeAlarm(activeAlarmForCancel);
            } else {
                ActiveAlarm activeAlarm = new ActiveAlarm(reminder.getDrugName(), reminderActiveAlarm);
                cancelAlarm(activeAlarm);
                ActiveAlarmsRepository.getInstance().removeAlarm(activeAlarm);
            }

        }
//        Intent intent = new Intent(context, AlarmsReceiver.class);
//        String drugName = reminder.getDrugName();
//        intent.setAction(drugName);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarmManager.cancel(pendingIntent);
    }

    private boolean isContainsSomeDrugsNames(ActiveAlarm activeAlarmForCancel) {
        String drugName = activeAlarmForCancel.getName();
        return drugName.contains(NAME_SPLITTER);
    }

    private ActiveAlarm splitAndRemoveDrugNameFromActiveAlarm(ActiveAlarm activeAlarmForCancel, String drugNameToRemove) {
        ActiveAlarm splittedAlarm = new ActiveAlarm(activeAlarmForCancel.getName(), activeAlarmForCancel.getTime());
        String fullName = splittedAlarm.getName();
        String[] allDrugNames = fullName.split(NAME_SPLITTER);
        ArrayList<String> drugNames = new ArrayList<>(Arrays.asList(allDrugNames));
        drugNames.remove(drugNameToRemove);
        if (drugNames.size() > 1) {
            StringBuilder nameBuilder = new StringBuilder();
            nameBuilder.append(drugNames.get(0));
            for (int i = 1; i < drugNames.size(); i++) {
                nameBuilder.append(NAME_SPLITTER);
                nameBuilder.append(drugNames.get(i));
            }
            splittedAlarm.setName(nameBuilder.toString());

        } else {
            splittedAlarm.setName(drugNames.get(0));
        }
        return splittedAlarm;
    }

    private void cancelAlarm(ActiveAlarm activeAlarm) {
        String drugName = activeAlarm.getName();
        Intent intent = new Intent(context, AlarmsReceiver.class);
        String action = drugName + (activeAlarm.getTime());
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        ActiveAlarmsRepository.getInstance().removeAlarm(activeAlarm);
    }

    private void setAlarmToPlanner(ActiveAlarm activeAlarm) {
        long testTime = Calendar.getInstance().getTimeInMillis();
        String drugName = activeAlarm.getName();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmsReceiver.class);
        intent.putExtra("drugname", drugName);
        String action = drugName + (activeAlarm.getTime()); //Unique string for pending intent identification in AlarmManager.
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, activeAlarm.getTime(), pendingIntent);
        }
    }

    public void reAlarmExpiredActiveAlarms(){ //TODO Relocate calling this for correct running (m.b. scheduler, or something like this).
        long currentTime = Calendar.getInstance().getTimeInMillis();
        ArrayList<ActiveAlarm> activeAlarms = ActiveAlarmsRepository.getInstance().getActiveAlarms();
        if (!activeAlarms.isEmpty()) {
            for (int i = 0; i < activeAlarms.size(); i++) {
                if (activeAlarms.get(i).getTime() < currentTime) {
                    ActiveAlarm reActivatingAlarm = activeAlarms.get(i);
                    long newTimeForNextActivating = reActivatingAlarm.getTime() + MILLISECONDS_IN_WEEK; //Expired task transfer to next week
                    reActivatingAlarm.setTime(newTimeForNextActivating);
                    activeAlarms.remove(i);
                    setAlarmToPlanner(reActivatingAlarm);
                    saveActiveAlarm(reActivatingAlarm);

                }
            }
        }
    }

//    private void cancelAlarm(Reminder reminder){
//        Intent intent = new Intent(context, AlarmsReceiver.class);
//        String action = reminder.getDrugName();
//        intent.setAction(action);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarmManager.cancel(pendingIntent);
//    }
//
//    private void setAlarmToPlanner(Reminder reminder, long alarmTimeInMilliseconds) {
//        String drugName = reminder.getDrugName();
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(context, AlarmsReceiver.class);
//        intent.putExtra("drugname", drugName);
//        String action = drugName;
//        intent.setAction(action);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
//        if (alarmManager != null) {
//            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTimeInMilliseconds, pendingIntent);
//        }
//    }
}
