package pro.kbgame.drugtimer.model;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import pro.kbgame.MyApplication;

class RemindersKeeper implements OnRemindersLoadCallback, OnRemindersSaveCallback {
    private static volatile RemindersKeeper instance = null;
    private final static String FILE_NAME = "reminders.txt";

    @NonNull
    public static RemindersKeeper getInstance() {
        RemindersKeeper remindersKeeper = instance;
        if (remindersKeeper == null) {
            synchronized (RemindersKeeper.class) {
                remindersKeeper = instance;
                if (remindersKeeper == null) {
                    remindersKeeper = instance = new RemindersKeeper();
                }
            }
        }
        return instance;
    }

    private void saveRemindersToFile(ArrayList<Reminder> reminders) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = MyApplication.getAppContext().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(reminders);
            objectOutputStream.flush();
            objectOutputStream.close();
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null)
                    fileOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    private ArrayList<Reminder> loadRemindersFromFile() {
        FileInputStream fileInputStream = null;
        ArrayList<Reminder> reminders = null;
        try {
            fileInputStream = MyApplication.getAppContext().openFileInput(FILE_NAME);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            reminders = (ArrayList<Reminder>) objectInputStream.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null)
                    fileInputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return reminders;
    }

    private ArrayList<Reminder> loadReminders() {
        ArrayList<Reminder> reminders = loadRemindersFromFile();
        if (reminders == null) {
            reminders = new ArrayList<Reminder>();
        }
        return reminders;
    }

    @Override
    public ArrayList<Reminder> onLoad() {
        return loadReminders();
    }

    @Override
    public void onLoadError() {

    }

    @Override
    public void onSave(ArrayList<Reminder> reminders) {
        saveRemindersToFile(reminders);
    }

    @Override
    public void onSaveError() {

    }
}
