package pro.kbgame.drugtimer.model;

import java.io.Serializable;

public class Time implements Serializable {

    private int hours;
    private int minutes;
    private boolean active;

    public Time(int hours, int minutes, boolean active) {
        this.hours = hours;
        this.minutes = minutes;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
}
