package pro.kbgame.drugtimer.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Reminder implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean isActive;
    private String drugName;
    private boolean[] turnonInWeekDay;
    private ArrayList<Time> timeList;

    public Reminder(boolean isActive, String drugName, boolean[] turnonInWeekDay, ArrayList<Time> timeList) {
        this.isActive = isActive;
        this.drugName = drugName;
        this.turnonInWeekDay = turnonInWeekDay;
        this.timeList = timeList;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean[] getTurnonInWeekDay() {
        return turnonInWeekDay;
    }

    public void setTurnonInWeekDay(boolean[] turnonInWeekDay) {
        this.turnonInWeekDay = turnonInWeekDay;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public ArrayList<Time> getTimeList() {
        return timeList;
    }

    public void setTimeList(ArrayList<Time> timeList) {
        this.timeList = timeList;
    }
}
