package pro.kbgame.drugtimer.model;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import pro.kbgame.MyApplication;

public class ActiveAlarmsRepository implements OnActiveAlarmsLoadCallback {
    private static volatile ActiveAlarmsRepository instance = null;
    private final static String FILE_NAME = "active_alarms.txt";
    private ArrayList<ActiveAlarm> activeAlarmsList = new ArrayList<>();
    private boolean inited = false;

    @NonNull
    public static ActiveAlarmsRepository getInstance() {
        ActiveAlarmsRepository activeAlarmsRepository = instance;
        if (activeAlarmsRepository == null) {
            synchronized (ActiveAlarmsRepository.class) {
                activeAlarmsRepository = instance;
                if (activeAlarmsRepository == null) {
                    activeAlarmsRepository = instance = new ActiveAlarmsRepository();
                }
            }
        }
        return instance;
    }

    public ActiveAlarmsRepository(){
        loadActiveAlarmsFromFile();
    }

    public void setActiveAlarm(ActiveAlarm activeAlarm){
        activeAlarmsList.add(activeAlarm);
        saveActiveAlarmsToFile(activeAlarmsList);

    }

    public ArrayList<ActiveAlarm> getActiveAlarms(){
        return activeAlarmsList;
    }

    private void saveActiveAlarmsToFile(ArrayList<ActiveAlarm> activeAlarms) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = MyApplication.getAppContext().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(activeAlarms);
            objectOutputStream.flush();
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void loadActiveAlarmsFromFile() {
        FileInputStream fileInputStream = null;
        ArrayList<ActiveAlarm> activeAlarms = new ArrayList<>();
        try {
            fileInputStream = MyApplication.getAppContext().openFileInput(FILE_NAME);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            activeAlarms = (ArrayList<ActiveAlarm>) objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        inited = true;
        activeAlarmsList = activeAlarms;
    }

    @Override
    public ArrayList<ActiveAlarm> onActiveAlarmsLoad() {
        return null;
    }

    @Override
    public void onActiveAlarmsLoadError() {

    }

    public void removeAlarm(ActiveAlarm activeAlarm) {
        activeAlarmsList.remove(activeAlarm);
        saveActiveAlarmsToFile(activeAlarmsList);
    }

    private void cleanUp() {
        long currentTime = Calendar.getInstance().getTimeInMillis();


    }
}
