package pro.kbgame.drugtimer.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class RemindersManager {
    private static volatile RemindersManager instance = null;
    private ArrayList<Reminder> remindersList;
    private static final int DAYS_IN_WEEK = 7;

    private RemindersManager() {
        remindersList = new ArrayList<>();
    }

    @NonNull
    public static RemindersManager getInstance() {
        RemindersManager remindersManager = instance;
        if (remindersManager == null) {
            synchronized (RemindersManager.class) {
                remindersManager = instance;
                if (remindersManager == null) {
                    remindersManager = instance = new RemindersManager();
                }
            }
        }
        return instance;
    }

    public void addReminderToList(Reminder reminder) {
        AlarmsManager.getInstance().setAlarms(reminder);
        remindersList.add(reminder);
        saveReminders();
    }

    public void removeReminderFromList(Reminder reminder) {
        AlarmsManager.getInstance().cancelAlarms(reminder);
        remindersList.remove(reminder);
        saveReminders();
    }

    public void saveReminderInList(int position, Reminder reminder) {
        AlarmsManager.getInstance().cancelAlarms(remindersList.get(position));
        remindersList.set(position, reminder);
        AlarmsManager.getInstance().setAlarms(reminder);
        saveReminders();
        AlarmsManager.getInstance().reAlarmExpiredActiveAlarms();
    }

    public void changeReminderActivityState(Reminder reminder){
        if(reminder.isActive()){
            AlarmsManager.getInstance().setAlarms(reminder);
        } else {
            AlarmsManager.getInstance().cancelAlarms(reminder);
        }
        saveReminders();
        AlarmsManager.getInstance().reAlarmExpiredActiveAlarms();
    }

    public Reminder getReminderFromList(int position) {
        if(remindersList.get(position) != null){
            return remindersList.get(position);
        }
        return null;
    }

    public ArrayList<Reminder> getRemindersList() {
        OnRemindersLoadCallback onRemindersLoadCallback = RemindersKeeper.getInstance();
        if (onRemindersLoadCallback.onLoad() != null) {
            remindersList = onRemindersLoadCallback.onLoad();
        }
        return remindersList;
    }

    public int getDAYS_IN_WEEK() {
        return DAYS_IN_WEEK;
    }

    public void saveReminders() {
        OnRemindersSaveCallback onRemindersSaveCallback = RemindersKeeper.getInstance();
        onRemindersSaveCallback.onSave(remindersList);
    }

    public void loadRemindersFromMemory(OnLoadData onLoadData) {
        onLoadData.onLoad(getRemindersList());
    }

    public interface OnLoadData {
        void onLoad(ArrayList<Reminder> reminders);
    }
}

