package pro.kbgame.drugtimer.model;

import java.util.ArrayList;

public interface OnActiveAlarmsLoadCallback {
    ArrayList<ActiveAlarm> onActiveAlarmsLoad();
    void onActiveAlarmsLoadError();
}
