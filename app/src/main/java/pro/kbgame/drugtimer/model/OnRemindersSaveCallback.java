package pro.kbgame.drugtimer.model;

import java.util.ArrayList;

public interface OnRemindersSaveCallback {
    void onSave(ArrayList<Reminder> reminders);
    void onSaveError();
}
