package pro.kbgame.drugtimer.model;

import java.util.ArrayList;

public interface OnRemindersLoadCallback {
    ArrayList<Reminder> onLoad();
    void onLoadError();
}
