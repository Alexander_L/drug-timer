package pro.kbgame.drugtimer.model;

import java.io.Serializable;

public class ActiveAlarm implements Serializable {
    private static final long serialVersionUID = 2L;

    private String name;
    private long time;

    public ActiveAlarm(String name, long time){
        this.name = name;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
