package pro.kbgame.drugtimer.utils;

import java.util.ArrayList;
import java.util.Calendar;

import pro.kbgame.drugtimer.model.Time;

public class AlarmsTimersPlanner {
    private final long MILLISECONDS_IN_MINUTE = 60000; // 60 sec * 1000 milliseconds
    private final long MILLISECONDS_IN_HOUR = 3600000; // 60 min * 60 sec * 1000 milliseconds
    private final long MILLISECONDS_IN_DAY = 86400000; // 24 h * 60 min *60 sec *1000 milliseconds
    private final long MILLISECONDS_IN_WEEK = 604800000; // 7d * 24 h * 60 min *60 sec *1000 milliseconds
    private boolean[] turnOnInWeekDay;
    private ArrayList<Time> reminderTimes;
    private ArrayList<Long> alarmsTimes;
    private long currentTime;
    private long currentWeekStart;


    public AlarmsTimersPlanner(boolean[] turnOnInWeekDay, ArrayList<Time> reminderTimes) {
        this.turnOnInWeekDay = turnOnInWeekDay;
        this.reminderTimes = reminderTimes;
        currentTime = Calendar.getInstance().getTimeInMillis();
        currentWeekStart = getCurrentWeekStart();
        alarmsTimes = new ArrayList<>();
    }

    private long getCurrentWeekStart() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public ArrayList<Long> getAlarmsTimesInMilliseconds() {
        ArrayList<Integer> activeDaysNumbers = getActiveDaysNumbers(turnOnInWeekDay);
        ArrayList<Time> activeTimes = parseActiveTimes(reminderTimes); //May check right away by time.isActive()
        ArrayList<Long> hoursAndSecondsInMills = convertTimesToMills(activeTimes);
        for (int dayNumber : activeDaysNumbers) {
            long dayAlarmTime = currentWeekStart + (dayNumber * MILLISECONDS_IN_DAY);
            for (Long hourAndMinute : hoursAndSecondsInMills) {
                long fullAlarmTime = dayAlarmTime + hourAndMinute;
                alarmsTimes.add(fullAlarmTime);
            }
        }
        replaceExpiredAlarmsOnNextWeek(alarmsTimes);
        return alarmsTimes;
    }

    private ArrayList<Integer> getActiveDaysNumbers(boolean[] turnOnInWeekDay) {
        ArrayList<Integer> daysNumbers = new ArrayList<>();
        for (int i = 0; i < turnOnInWeekDay.length; i++) {
            if (turnOnInWeekDay[i]) {
                daysNumbers.add(i);
            }
        }
        return daysNumbers;
    }

    private ArrayList<Time> parseActiveTimes(ArrayList<Time> reminderTimes) {
        ArrayList<Time> activeTimes = new ArrayList<>();
        for (Time time : reminderTimes) {
            if (time.isActive()) {
                activeTimes.add(time);
            }
        }
        return activeTimes;
    }

    private ArrayList<Long> convertTimesToMills(ArrayList<Time> activeTimes) {
        ArrayList<Long> times = new ArrayList<>();
        long alarmHoursAndMinutesInMilliseconds;
        for (Time time : activeTimes) {
            alarmHoursAndMinutesInMilliseconds = (time.getHours() * MILLISECONDS_IN_HOUR) + (time.getMinutes() * MILLISECONDS_IN_MINUTE);
            times.add(alarmHoursAndMinutesInMilliseconds);
        }
        return times;
    }

    private void replaceExpiredAlarmsOnNextWeek(ArrayList<Long> alarmsTimes) {
        for (int i = 0; i < alarmsTimes.size(); i++) {
            if (alarmsTimes.get(i) <= currentTime) {
                long correctedTime = alarmsTimes.get(i) + MILLISECONDS_IN_WEEK;
                alarmsTimes.set(i, correctedTime);
            }
        }
    }
}

//region previous version for control
//    public ArrayList<Long> getAlarmsTimesInMilliseconds() {
//        long startPointTime = getTodayStartTimeInMilliseconds();
//        ArrayList<Long> alarmsTimes = new ArrayList<>();
//        ArrayList<Long> timeOfDays = getActiveDaysOfWeekStart();
//        ArrayList<Long> alarmAtDayTimes = parseActiveTimesFromReminder();
//        for (long dayTime : timeOfDays) {
//            for (int i = 0; i < alarmAtDayTimes.size(); i++) {
//                long alarmTime = startPointTime + dayTime + alarmAtDayTimes.get(i);
//                alarmsTimes.add(alarmTime);
//            }
//        }
//        alarmsTimes = checkExpiredAlarmsAndReAlarmOnNextWeek(alarmsTimes);
//        return alarmsTimes;
//    }
//
//    private ArrayList<Long> getActiveDaysOfWeekStart() {
//        int currentDayNumber = getCurrentDayInWeekNumber(); //Sunday == 7; Monday == 1 etc.
//        ArrayList<Long> alarmsTimes = new ArrayList<>();
//        //Think here. Maybe simplify to
//        // int currentDayNumber = getCurrentDayInWeekNumber() - 1;
//        // and remove added variable "dayNumber" in next cycle;
//
//        for (int i = 0; i < turnOnInWeekDay.length; i++) { //boolean[] turnOnInWeekDay starts at 0 index for monday value, ends at index 6 for sunday.
//            int planeDayNumber = i + 1;  //This equalizes iterator with currentDayNumber.
//            if (turnOnInWeekDay[i]) {
//                if (planeDayNumber < currentDayNumber) {
//                    long nextWeekBeginningInMilliseconds = getNextWeekBeginningInMilliseconds();
//                    long dayInWeekToAlarmBeginningInMilliseconds = planeDayNumber * MILLISECONDS_IN_DAY;
//                    long alarmTimeInMilliseconds = nextWeekBeginningInMilliseconds + dayInWeekToAlarmBeginningInMilliseconds;
//                    alarmsTimes.add(alarmTimeInMilliseconds);
//                    //calculate difference and plane for next week
//                } else if (planeDayNumber > currentDayNumber) {
//                    int daysToAlarm = planeDayNumber - currentDayNumber;
//                    long dayInWeekToAlarmBeginningInMilliseconds = daysToAlarm * MILLISECONDS_IN_DAY;
//                    alarmsTimes.add(dayInWeekToAlarmBeginningInMilliseconds);
//                    //calculate difference and plane for current week
//                } else {
//                    long todayBeginningTimeInMilliseconds = 0;
//                    alarmsTimes.add(todayBeginningTimeInMilliseconds);
//                    //test for past, or present, or future time of today and plane to execute
//                }
//            }
//        }
//        return alarmsTimes;
//    }
//
//    private long getNextWeekBeginningInMilliseconds() {
//        Calendar calendar = Calendar.getInstance();
//        int currentWeekInYear = calendar.get(Calendar.WEEK_OF_YEAR);
//        int nextWeekInYear = 0;
//        if (currentWeekInYear <= 51) {
//            nextWeekInYear = currentWeekInYear + 1;
//
//        } else {
//            int currentYear = calendar.get(Calendar.YEAR);
//            int nextYear = currentYear + 1;
//            nextWeekInYear = 1;
//            calendar.set(Calendar.YEAR, nextYear);
//        }
//        calendar.set(Calendar.WEEK_OF_YEAR, nextWeekInYear);
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        return calendar.getTimeInMillis();
//
//
//        // Look and try to work with milliseconds only
//
//    }
//
//    private ArrayList<Long> parseActiveTimesFromReminder() {
//        ArrayList<Long> times = new ArrayList<>();
//        long alarmHoursAndMinutesInMilliseconds = 0L;
//        for (Time time : reminderTimes) {
//            if (time.isActive()) {
//                alarmHoursAndMinutesInMilliseconds = convertTimeToMilliseconds(time);
//                times.add(alarmHoursAndMinutesInMilliseconds);
//            }
//        }
//        return times;
//    }
//
//    private ArrayList<Long> checkExpiredAlarmsAndReAlarmOnNextWeek(ArrayList<Long> alarmsTimes) {
//        long currentTime = System.currentTimeMillis();
//        for (int i = 0; i < alarmsTimes.size(); i++) {
//            if (alarmsTimes.get(i) < currentTime) {
//                long alarmTimeOnNextWeek = alarmsTimes.get(i) + MILLISECONDS_IN_WEEK;
//                alarmsTimes.set(i, alarmTimeOnNextWeek);
//            }
//        }
//        return alarmsTimes;
//    }
//
//
//    private int getCurrentDayInWeekNumber() {
//        int dayInWeekNumber = 0;
//        Calendar calendar = Calendar.getInstance();
//        // correcting day number. Week will starts from Monday == 1, ends Sunday == 7.
//        int originDayNumber = calendar.get(Calendar.DAY_OF_WEEK);
//        if (originDayNumber == 1) {
//            dayInWeekNumber = originDayNumber + 6;
//        } else {
//            dayInWeekNumber = originDayNumber - 1;
//        }
//        return dayInWeekNumber;
//    }
//
//    private long convertTimeToMilliseconds(Time time) {
//        long timeInMilliseconds = 0L;
//        long hoursInMilliseconds = TimeUnit.HOURS.toMillis(time.getHours());
//        long minutesInMilliseconds = TimeUnit.MINUTES.toMillis(time.getMinutes());
//        timeInMilliseconds = hoursInMilliseconds + minutesInMilliseconds;
//        return timeInMilliseconds;
//    }
//
//    private long getTodayStartTimeInMilliseconds() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        return calendar.getTimeInMillis();
//    }
//}
//endregion