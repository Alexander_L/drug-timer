package pro.kbgame.drugtimer.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public class AlarmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DrugTimer:alarm");
        String name = "";
        if (intent != null && intent.getStringExtra("drugname") != null) {
            name = intent.getStringExtra("drugname");
        }

        wakeLock.acquire();

        Intent alertIntent = new Intent();
        alertIntent.setClassName("pro.kbgame.drugtimer", "pro.kbgame.drugtimer.view.AlertActivity");
        alertIntent.putExtra("drugname", name);
        alertIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(alertIntent);

        wakeLock.release();
    }
}
