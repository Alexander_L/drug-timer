package pro.kbgame.drugtimer.utils;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class LocalWeekDaysNames {
    private static volatile LocalWeekDaysNames instance = null;

    @NonNull
    public static LocalWeekDaysNames getInstance(){
        LocalWeekDaysNames localWeekDaysNames = instance;
        if(localWeekDaysNames == null){
            synchronized (LocalWeekDaysNames.class){
                localWeekDaysNames = instance;
                if(localWeekDaysNames == null){
                    localWeekDaysNames = instance = new LocalWeekDaysNames();
                }
            }
        }
        return instance;
    }

    public ArrayList<String> getDaysNames(String length){ //TODO Ask about transferring parameter
        String dayNamePattern = "";
        if(length.equals("short")){
            dayNamePattern = "E";
        } else {
            dayNamePattern = "EEEE";
        }
        ArrayList<String> weekDaysNames = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat(dayNamePattern);
        String dayName = simpleDateFormat.format(calendar.getTime());
        weekDaysNames.add(dayName);
        if(calendar.get(Calendar.DAY_OF_WEEK) == 2) {
            for (int i = 3; i <= 7; i++) {
                calendar.set(Calendar.DAY_OF_WEEK, i);
                dayName = simpleDateFormat.format(calendar.getTime());
                weekDaysNames.add(dayName);
            }
            calendar.set(Calendar.DAY_OF_WEEK, 1);
            dayName = simpleDateFormat.format(calendar.getTime());
            weekDaysNames.add(dayName);
        } else {
            for (int i = 2; i <= 7; i++) {
                calendar.set(Calendar.DAY_OF_WEEK, i);
                dayName = simpleDateFormat.format(calendar.getTime());
                weekDaysNames.add(dayName);
            }
        }
        return weekDaysNames;
    }
}
