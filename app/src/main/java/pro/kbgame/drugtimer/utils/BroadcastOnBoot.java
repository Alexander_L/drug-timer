package pro.kbgame.drugtimer.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import pro.kbgame.drugtimer.model.ActiveAlarm;
import pro.kbgame.drugtimer.model.ActiveAlarmsRepository;
import pro.kbgame.drugtimer.model.AlarmsManager;

public class BroadcastOnBoot extends BroadcastReceiver {
    private static final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_BOOT.equals(intent.getAction())) {
            ArrayList<ActiveAlarm> activeAlarmsLoaded = ActiveAlarmsRepository.getInstance().getActiveAlarms();
            AlarmsManager.getInstance().planAlarms(activeAlarmsLoaded);
        }
    }
}